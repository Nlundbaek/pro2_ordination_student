package JunitTestOrdination;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.time.LocalDate;
import java.time.LocalTime;

import org.junit.Before;
import org.junit.Test;

import ordination.Dosis;
import ordination.Laegemiddel;
import ordination.PN;
import ordination.Patient;
import service.Service;

public class ServiceTest {
	private Service service;
	private Patient patient;
	private Laegemiddel lægemiddel;
	private Dosis dosis;
	private PN ordination;

	@Before
	public void setUp() {

		service = Service.getService();
		patient = new Patient("123456-7890", "Karl Johan", 78.2);
		lægemiddel = new Laegemiddel("Panodil", 2.1, 3.2, 4.3, "mg");
		dosis = new Dosis(LocalTime.of(13, 39), 3);
		ordination = new PN(LocalDate.of(2016, 02, 19), LocalDate.of(2016, 02, 22), lægemiddel, 2.0);
	}

	@Test
	public void testOpretPNOrdination() {
		assertNotNull(service.opretPNOrdination(LocalDate.of(2016, 02, 16), LocalDate.of(2016, 02, 18), patient,
				lægemiddel, 3));
	}

	@Test
	public void testOpretDagligFastOrdination() {
		assertNotNull(service.opretDagligFastOrdination(LocalDate.of(2016, 02, 19), LocalDate.of(2016, 02, 22), patient,
				lægemiddel, 2, 1, 1, 1));
	}

	@Test
	public void testOpretDagligSkaevOrdination() {
		double[] antal = new double[1];
		antal[0] = 3.0;
		LocalTime[] time = new LocalTime[1];
		time[0] = LocalTime.of(13, 30);
		assertNotNull(service.opretDagligSkaevOrdination(LocalDate.of(2016, 02, 19), LocalDate.of(2016, 02, 22),
				patient, lægemiddel, time, antal));
	}

	@Test(expected = IllegalArgumentException.class)
	public void testOrdinationPNAnvendt() {
		LocalDate ld = LocalDate.of(2016, 02, 26);
		service.ordinationPNAnvendt(ordination, ld);
		service.ordinationPNAnvendt(ordination, ld);
		assertEquals(4, ordination.samletDosis(), 0.001);
	}

	@Test
	public void testAnbefaletDosisPrDoegn() {
		assertEquals(250.24, service.anbefaletDosisPrDoegn(patient, lægemiddel), 0.001);
	}

	@Test
	public void testAntalOrdinationerPrVægtPrLægemiddel() {
		assertEquals(0, service.antalOrdinationerPrVægtPrLægemiddel(78.4, 87.2, lægemiddel));
	}

	@Test
	public void testGetAllPatienter() {
		assertNotNull(service.getAllPatienter());
	}

	@Test
	public void testGetAllLaegemidler() {
		assertNotNull(service.getAllLaegemidler());
	}

}
