package JunitTestOrdination;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.time.LocalDate;
import java.time.LocalTime;

import org.junit.Before;
import org.junit.Test;

import ordination.DagligFast;
import ordination.Dosis;
import ordination.Laegemiddel;

//Junit test of DagligFast methods
public class DagligFastTest1 {

	private Laegemiddel middel;
	private DagligFast dagligF;
	private Dosis[] dosiser = new Dosis[4];

	@Before
	public void setUp() throws Exception {
		middel = new Laegemiddel("Acetylsalicylsyre", 0.1, 0.15, 0.16, "Styk");
		dagligF = new DagligFast(LocalDate.of(2016, 2, 13), LocalDate.of(2016, 3, 13), middel, 1, 1, 1, 1);
		dosiser[0] = new Dosis(LocalTime.of(9, 0), 1);
		dosiser[1] = new Dosis(LocalTime.of(12, 0), 1);
		dosiser[2] = new Dosis(LocalTime.of(18, 0), 1);
		dosiser[3] = new Dosis(LocalTime.of(23, 0), 1);
	}

	@Test
	public void testSamletDosis() {
		assertEquals(120, dagligF.samletDosis(), 0.000001);
	}

	@Test
	public void testDoegnDosis() {
		assertEquals(4, dagligF.doegnDosis(), 0.000001);
	}

	@Test
	public void testGetType() {
		String s = "DagligFast";
		assertTrue(s.equals(dagligF.getType()));
	}

	@Test(expected = IllegalArgumentException.class)
	public void testDagligFast() {
		DagligFast dagligFail = new DagligFast(LocalDate.of(2016, 2, 13), LocalDate.of(2016, 3, 13), middel, 1, -1, 1,
				1);
		;
	}

	@Test
	public void testGetDoser() {
		assertEquals(dosiser[0].getAntal(), dagligF.getDoser()[0].getAntal(), 0.0001);
		assertEquals(dosiser[1].getAntal(), dagligF.getDoser()[1].getAntal(), 0.0001);
		assertEquals(dosiser[2].getAntal(), dagligF.getDoser()[2].getAntal(), 0.0001);
		assertEquals(dosiser[3].getAntal(), dagligF.getDoser()[3].getAntal(), 0.0001);
		assertTrue(dosiser[0].getTid().equals(dagligF.getDoser()[0].getTid()));
		assertTrue(dosiser[1].getTid().equals(dagligF.getDoser()[1].getTid()));
		assertTrue(dosiser[2].getTid().equals(dagligF.getDoser()[2].getTid()));
		assertTrue(dosiser[3].getTid().equals(dagligF.getDoser()[3].getTid()));

	}

	@Test
	public void testDeleteDosis() {
		DagligFast dagligFLocal = dagligF;
		dagligFLocal.deleteDosis(0);
		assertNull(dagligFLocal.getDoser()[0]);

	}

}
