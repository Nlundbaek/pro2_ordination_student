package JunitTestOrdination;

import static org.junit.Assert.*;

import java.time.LocalDate;

import org.junit.Before;
import org.junit.Test;

import ordination.DagligSkaev;
import ordination.Laegemiddel;
import ordination.PN;

public class PNTest {

	Laegemiddel laegemiddel = new Laegemiddel("Acetylsalicylsyre", 0.1, 0.15, 0.16, "Styk");
	PN pn = new PN(LocalDate.of(2016, 02, 15), LocalDate.of(2016, 02, 18), laegemiddel, 10);
	
	@Before
	public void setUp() {
		
	}
	
	@Test
	public void testSamletDosis() {
		pn.givDosis(LocalDate.of(2016, 02, 16));
		pn.givDosis(LocalDate.of(2016, 02, 17));
		pn.givDosis(LocalDate.of(2016, 02, 18));
		assertEquals(30, pn.samletDosis(), 0.01);
		
	}

	@Test
	public void testDoegnDosis() {
		pn.givDosis(LocalDate.of(2016, 02, 16));
		pn.givDosis(LocalDate.of(2016, 02, 17));
		pn.givDosis(LocalDate.of(2016, 02, 17));
		assertEquals( 15,pn.doegnDosis(),0.01);
		
	}

	@Test
	public void testGetType() {
		assertEquals("PN",pn.getType());
	}



	@Test
	public void testGivDosis() {
		pn.givDosis(LocalDate.of(2016, 02, 16));
	}

	@Test
	public void testGetAntalGangeGivet() {
		pn.givDosis(LocalDate.of(2016, 02, 16));
		pn.givDosis(LocalDate.of(2016, 02, 16));
		pn.givDosis(LocalDate.of(2016, 02, 16));
		pn.givDosis(LocalDate.of(2016, 02, 16));
		pn.givDosis(LocalDate.of(2016, 02, 16));
		
		assertEquals(5 , pn.getAntalGangeGivet(), 0.01);
	}

	@Test
	public void testGetAntalEnheder() {
		assertEquals(10, pn.getAntalEnheder(), 0.01);
	}

}
