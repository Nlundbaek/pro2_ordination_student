package JunitTestOrdination;

import static org.junit.Assert.*;

import java.time.LocalDate;
import java.time.LocalTime;

import org.junit.Before;
import org.junit.Test;
import ordination.DagligSkaev;
import ordination.Laegemiddel;
public class DagligSkaevTest {
	
	Laegemiddel laegemiddel = new Laegemiddel("Acetylsalicylsyre", 0.1, 0.15, 0.16, "Styk");
	DagligSkaev ds = new DagligSkaev(LocalDate.of(2016, 1, 23), LocalDate.of(2016, 1, 24), laegemiddel);
	
	@Before
	public void Setup(){
		ds.opretDosis(LocalTime.of(10,30), 20);
	
	}
	
	@Test
	public void testSamletDosis() {
	assertEquals(40, ds.samletDosis(),0.01);
	}

	@Test
	public void testDoegnDosis() {
		assertEquals(20, ds.doegnDosis(), 0.01);
	}

	@Test
	public void testGetType() {
		assertTrue(ds.getType() == "DagligSkaev");
	}

//	@Test
//	public void testDagligSkaev() {
//		
//	}

	@Test
	public void testOpretDosis() {
		ds.opretDosis(LocalTime.of(9,30), 10);
		assertTrue(ds.getDoser().size() == 2);
	}

	@Test
	public void testGetDoser() {
		assertTrue(ds.getDoser().get(0) != null);
	}

	@Test
	public void testRemoveDosis() {
		ds.removeDosis(ds.getDoser().get(0));
		assertTrue(ds.getDoser().size() == 0);
	
	}

}
