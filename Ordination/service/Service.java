package service;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.List;

import ordination.DagligFast;
import ordination.DagligSkaev;
import ordination.Laegemiddel;
import ordination.Ordination;
import ordination.PN;
import ordination.Patient;
import storage.Storage;

public class Service {
	private Storage storage;
	private static Service service;

	private Service() {
		storage = new Storage();
	}

	public static Service getService() {
		if (service == null) {
			service = new Service();
		}
		return service;
	}

	public static Service getTestService() {
		return new Service();
	}

	/**
	 * Hvis startDato er efter slutDato kastes en IllegalArgumentException og
	 * ordinationen oprettes ikke
	 *
	 * @return opretter og returnerer en PN ordination.
	 */
	public PN opretPNOrdination(LocalDate startDen, LocalDate slutDen, Patient patient, Laegemiddel laegemiddel,
			double antal) {
		if (startDen.isAfter(slutDen)) {
			throw new IllegalArgumentException("Illegal dato");
		}

		PN pn = new PN(startDen, slutDen, laegemiddel, antal);
		patient.addOrdination(pn);
		return pn;
	}

	/**
	 * Opretter og returnerer en DagligFast ordination. Hvis startDato er efter
	 * slutDato kastes en IllegalArgumentException og ordinationen oprettes ikke
	 */
	public DagligFast opretDagligFastOrdination(LocalDate startDen, LocalDate slutDen, Patient patient,
			Laegemiddel laegemiddel, double morgenAntal, double middagAntal, double aftenAntal, double natAntal) {
		// TODO
		if (startDen.isAfter(slutDen)) {
			throw new IllegalArgumentException("Ugyldig Dato");
		}

		DagligFast df = new DagligFast(startDen, slutDen, laegemiddel, morgenAntal, middagAntal, aftenAntal, natAntal);
		patient.addOrdination(df);
		return df;
	}

	/**
	 * Opretter og returnerer en DagligSkæv ordination. Hvis startDato er efter
	 * slutDato kastes en IllegalArgumentException og ordinationen oprettes ikke
	 */
	public DagligSkaev opretDagligSkaevOrdination(LocalDate startDen, LocalDate slutDen, Patient patient,
			Laegemiddel laegemiddel, LocalTime[] klokkeSlet, double[] antalEnheder) {

		if (startDen.isAfter(slutDen)) {
			throw new IllegalArgumentException("Ugyldig Dato");
		}

		DagligSkaev ds = new DagligSkaev(startDen, slutDen, laegemiddel);
		for (int i = 0; i < antalEnheder.length; i++) {
			ds.opretDosis(klokkeSlet[i], antalEnheder[i]);
		}
		patient.addOrdination(ds);
		return ds;
	}

	/**
	 * En dato for hvornår ordinationen anvendes tilføjes ordinationen. Hvis
	 * datoen ikke er indenfor ordinationens gyldighedsperiode kastes en
	 * IllegalArgumentException
	 */
	public void ordinationPNAnvendt(PN ordination, LocalDate dato) {

		if (ordination.givDosis(dato)) {
		} else {
			throw new IllegalArgumentException("Ugyldig Dato");
		}
	}

	/**
	 * Den anbefalede dosis for den pågældende patient (der skal tages hensyn
	 * til patientens vægt). Det er en forskellig enheds faktor der skal
	 * anvendes, og den er afhængig af patientens vægt.
	 */
	public double anbefaletDosisPrDoegn(Patient patient, Laegemiddel laegemiddel) {
		double result;
		if (patient.getVaegt() < 25) {
			result = patient.getVaegt() * laegemiddel.getEnhedPrKgPrDoegnLet();
		} else if (patient.getVaegt() > 120) {
			result = patient.getVaegt() * laegemiddel.getEnhedPrKgPrDoegnTung();
		} else {
			result = patient.getVaegt() * laegemiddel.getEnhedPrKgPrDoegnNormal();
		}
		return result;
	}

	/**
	 * For et givent vægtinterval og et givent lægemiddel, hentes antallet af
	 * ordinationer.
	 */
	public int antalOrdinationerPrVægtPrLægemiddel(double vægtStart, double vægtSlut, Laegemiddel laegemiddel) {
		int count = 0;
		for (Patient elementPa : storage.getAllPatienter()) {
			if (elementPa.getVaegt() >= vægtStart && elementPa.getVaegt() <= vægtSlut) {
				for (Ordination elementOr : elementPa.getOrdinationer()) {
					if (elementOr.getLaegemiddel().equals(laegemiddel)) {
						count++;
					}
				}
			}
		}
		return count;
	}

	public List<Patient> getAllPatienter() {
		return storage.getAllPatienter();
	}

	public List<Laegemiddel> getAllLaegemidler() {
		return storage.getAllLaegemidler();
	}

	/**
	 * Metode der kan bruges til at checke at en startDato ligger før en
	 * slutDato.
	 *
	 * @return true hvis startDato er før slutDato, false ellers.
	 */
	private boolean checkStartFoerSlut(LocalDate startDato, LocalDate slutDato) {
		boolean result = true;
		if (slutDato.compareTo(startDato) < 0) {
			result = false;
		}
		return result;
	}

	public void createSomeObjects() {
		storage.addPatient(new Patient("Jane Jensen", "121256-0512", 63.4));
		storage.addPatient(new Patient("Finn Madsen", "070985-1153", 83.2));
		storage.addPatient(new Patient("Hans Jørgensen", "050972-1233", 89.4));
		storage.addPatient(new Patient("Ulla Nielsen", "011064-1522", 59.9));
		storage.addPatient(new Patient("Ib Hansen", "090149-2529", 87.7));

		storage.addLaegemiddel(new Laegemiddel("Acetylsalicylsyre", 0.1, 0.15, 0.16, "Styk"));
		storage.addLaegemiddel(new Laegemiddel("Paracetamol", 1, 1.5, 2, "Ml"));
		storage.addLaegemiddel(new Laegemiddel("Fucidin", 0.025, 0.025, 0.025, "Styk"));
		storage.addLaegemiddel(new Laegemiddel("Methotrexat", 0.01, 0.015, 0.02, "Styk"));

		opretPNOrdination(LocalDate.of(2016, 1, 1), LocalDate.of(2016, 1, 12), storage.getAllPatienter().get(0),
				storage.getAllLaegemidler().get(1), 123);

		opretPNOrdination(LocalDate.of(2016, 2, 12), LocalDate.of(2016, 2, 14), storage.getAllPatienter().get(0),
				storage.getAllLaegemidler().get(0), 3);

		opretPNOrdination(LocalDate.of(2016, 1, 20), LocalDate.of(2016, 1, 25), storage.getAllPatienter().get(3),
				storage.getAllLaegemidler().get(2), 5);

		opretPNOrdination(LocalDate.of(2016, 1, 1), LocalDate.of(2016, 1, 12), storage.getAllPatienter().get(0),
				storage.getAllLaegemidler().get(1), 123);

		opretDagligFastOrdination(LocalDate.of(2016, 1, 10), LocalDate.of(2016, 1, 12),
				storage.getAllPatienter().get(1), storage.getAllLaegemidler().get(1), 2, 1, 1, 1);

		LocalTime[] kl = { LocalTime.of(12, 0), LocalTime.of(12, 40), LocalTime.of(16, 0), LocalTime.of(18, 45) };
		double[] an = { 0.5, 1, 2.5, 3 };

		opretDagligSkaevOrdination(LocalDate.of(2016, 1, 23), LocalDate.of(2016, 1, 24),
				storage.getAllPatienter().get(1), storage.getAllLaegemidler().get(2), kl, an);
	}

}
