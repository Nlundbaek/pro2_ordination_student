package ordination;

import java.time.LocalDate;
import java.time.LocalTime;
import java.time.temporal.ChronoUnit;

public class DagligFast extends Ordination {

	private Dosis[] dosises = new Dosis[4];

	public DagligFast(LocalDate startDen, LocalDate slutDen, Laegemiddel laegemiddel, double morgenAntal,
			double middagAntal, double aftenAntal, double natAntal) {
		super(startDen, slutDen, laegemiddel);

		if (morgenAntal >= 0 && middagAntal >= 0 && aftenAntal >= 0 && natAntal >= 0) {
			dosises[0] = new Dosis(LocalTime.of(9, 0), morgenAntal);
			dosises[1] = new Dosis(LocalTime.of(12, 0), middagAntal);
			dosises[2] = new Dosis(LocalTime.of(18, 0), aftenAntal);
			dosises[3] = new Dosis(LocalTime.of(23, 0), natAntal);
		} else {
			throw new IllegalArgumentException("Ugyldig input");
		}
	}

	@Override
	public double samletDosis() {
		int samletDosis = (int) this.doegnDosis();
		int antalDage = (int) ChronoUnit.DAYS.between(getStartDen(), getSlutDen()) + 1;

		return (double) samletDosis * (double) antalDage;
	}

	@Override
	public double doegnDosis() {
		if (dosises[0] == null) {
			return 0;
		}

		double antal = 0;
		for (int i = 0; i < dosises.length; i++) {
			antal += dosises[i].getAntal();
		}
		return antal;
	}

	@Override
	public String getType() {
		return "DagligFast";
	}

	public Dosis[] getDoser() {
		return dosises;
	}

	public void deleteDosis(int timePOS) {
		dosises[timePOS] = null;
	}

}
