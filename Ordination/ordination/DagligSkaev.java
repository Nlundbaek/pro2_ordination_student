package ordination;

import java.time.LocalDate;
import java.time.LocalTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;

public class DagligSkaev extends Ordination {

	private ArrayList<Dosis> dosis = new ArrayList<>();

	public DagligSkaev(LocalDate startDen, LocalDate slutDen, Laegemiddel laegemiddel) {
		super(startDen, slutDen, laegemiddel);
	}

	public void opretDosis(LocalTime tid, double antal) {
		if (antal <= 0 || tid.getHour() <= 0) {
			throw new IllegalArgumentException("Ugyldig input");
		}
		dosis.add(new Dosis(tid, antal));

	}

	public ArrayList<Dosis> getDoser() {
		return new ArrayList<>(dosis);
	}

	public void removeDosis(Dosis Dosis) {
		dosis.remove(Dosis);
	}

	@Override
	public double samletDosis() {
		double samletdosis = 0;
		int antalDage = (int) ChronoUnit.DAYS.between(getStartDen(), getSlutDen()) + 1;
		// husk at checke antal dage er korrekt
		for (int i = 0; i < dosis.size(); i++) {
			samletdosis += dosis.get(i).getAntal();
		}

		return samletdosis * antalDage;
	}

	@Override
	public double doegnDosis() {
		double dagligdosis = 0;

		for (int i = 0; i < dosis.size(); i++) {
			dagligdosis += dosis.get(i).getAntal();
		}

		return dagligdosis;
	}

	@Override
	public String getType() {

		return "DagligSkaev";
	}
}
