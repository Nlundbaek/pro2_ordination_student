package ordination;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;

public class PN extends Ordination {

	private double antalEnheder;
	private int antalGangeGivet = 0;
	private ArrayList<LocalDate> dates = new ArrayList<>();

	public PN(LocalDate startDen, LocalDate slutDen, Laegemiddel laegemiddel, double antalEnheder) {
		super(startDen, slutDen, laegemiddel);
		this.antalEnheder = antalEnheder;
	}

	/**
	 * Registrerer at der er givet en dosis paa dagen givesDen Returnerer true
	 * hvis givesDen er inden for ordinationens gyldighedsperiode og datoen
	 * huskes Retrurner false ellers og datoen givesDen ignoreres
	 * 
	 * @param givesDen
	 * @return
	 */
	public boolean givDosis(LocalDate givesDen) {
		
		if ( givesDen == null){
			return false;
		}
		if (givesDen.isAfter(getStartDen()) && givesDen.isBefore(getSlutDen()) || givesDen.isEqual(getSlutDen())
				|| givesDen.isEqual(getStartDen())) {
			dates.add(givesDen);
			antalGangeGivet++;
			return true;
		}

		else {

			return false;
		}

	}

	@Override
	public double doegnDosis() {
		if ( dates.size() == 0) {
			return 0;
		}
		
		if ( dates.size() == 1 ) { 
			return antalGangeGivet * antalEnheder;
		}
		
		else {
		int days = (int) ChronoUnit.DAYS.between(dates.get(0), dates.get(dates.size() - 1))+1;
		return antalGangeGivet * antalEnheder / days;
		
		}
	}

	@Override
	public double samletDosis() {

		return  antalGangeGivet * antalEnheder;

	}

	/**
	 * Returnerer antal gange ordinationen er anvendt
	 * 
	 * @return
	 */
	public int getAntalGangeGivet() {

		return antalGangeGivet;
	}

	public double getAntalEnheder() {
		return antalEnheder;
	}

	@Override
	public String getType() {

		return "PN";
	}

}
